import { Router, Request, Response } from 'express';
import Server from '../classes/server';
import { GraficaData } from '../classes/grafica';

import {User} from '../classes/user';

export const router = Router();
const grafica = new GraficaData();
const user = new User();

router.get('/grafica', (req: Request, res: Response) => {
    res.json(grafica.getDataGrafica());
});

router.get('/user', async(req: Request, res: Response) => {
    const users = await user.getUserInactive()
    try {
        res.json({
            ok: true,
            body : {
                users
            }
        })
    } catch (err) {
        return res.status(500).send('Error en el servidor');
    }
});

router.post('/user', async (req: Request, res: Response) => {
    // const mes = req.body.mes;
    // const unidades = Number(req.body.unidades);
    const {username, active} = req.body;
    const users = await user.changeUserStatus(username, active);

    console.log('post user ', req.body, users);

    const server = Server.instance;
    server.io.emit('change-user', users);
    res.json(users);
});

router.post('/grafica', (req: Request, res: Response) => {
    const mes = req.body.mes;
    const unidades = Number(req.body.unidades);
    grafica.incrementarValor(mes, unidades);

    const server = Server.instance;
    server.io.emit('cambio-grafica', grafica.getDataGrafica());

    res.json(grafica.getDataGrafica());
});

router.post('/mensajes/:id', (req: Request, res: Response) => {
    const cuerpo = req.body.cuerpo;
    const de = req.body.de;
    const id = req.params.id;
    const payload = {
        de,
        cuerpo
    };

    const server = Server.instance;
    server.io.in(id).emit('mensaje-privado', payload)

    res.json({
        ok: true,
        cuerpo,
        de,
        id
    });
});