// const conexion = require('../connection');
import {mysql} from '../connection';

export class User {
    constructor() {}
 
    getUserInactive() {
        return new Promise((resolve, reject) => {
            mysql.query(`SELECT u.username, u.active FROM auth_user u WHERE u.username <> '' AND u.active IS FALSE ORDER BY u.username LIMIT 10000`,
            (err: Error, response: Response) => {
                // console.log(err, response);
                if (err) reject(err);
                else resolve(response);
            });
        });
    }

    getCounterUserInactive() {
        return new Promise((resolve, reject) => {
            mysql.query(`SELECT COUNT(u.id) AS counter  FROM auth_user u WHERE u.username <> '' AND u.active IS FALSE ORDER BY u.username LIMIT 10000`,
            (err: Error, response: Response) => {
                // console.log(err, response);
                if (err) reject(err);
                else resolve(response);
            });
        });
    }

    changeUserStatus(username: string, active: boolean) {
        return new Promise((resolve, reject) => {
            const isActive = active ? `b'1'`: `b'0'`;
            console.log('active ', active);

            mysql.query(`UPDATE auth_user u SET u.active = ${isActive} WHERE u.username= '${username}'`,
            (err: Error, response: Response) => {
                console.log(err, response);
                if (err) reject(err);
                else resolve(this.getUserInactive());
            });
        });

        // mes = mes.toLowerCase().trim();
        // for (const i in this.meses) {
        //     if(this.meses[i] === mes) {
        //         this.valores[i] += valor;
        //     }
        // }
        // return this.getDataGrafica();
    }
}

// module.exports = {
    
    // obtenerPorId(id) {
    //     return new Promise((resolve, reject) => {
    //         conexion.query(`select id, nombre, precio from not_email_sent where id = ?`,
    //             [id],
    //             (err, resultados) => {
    //                 console.log({resultados});
    //                 if (err) reject(err);
    //                 else resolve(resultados[0]);
    //             });
    //     });
    // },
    // actualizar(id, nombre, precio) {
    //     return new Promise((resolve, reject) => {
    //         conexion.query(`update not_email_sent
    //         set nombre = ?,
    //         precio = ?
    //         where id = ?`,
    //             [nombre, precio, id],
    //             (err) => {
    //                 if (err) reject(err);
    //                 else resolve();
    //             });
    //     });
    // },
    // eliminar(id) {
    //     return new Promise((resolve, reject) => {
    //         conexion.query(`delete from not_email_sent
    //         where id = ?`,
    //             [id],
    //             (err) => {
    //                 if (err) reject(err);
    //                 else resolve();
    //             });
    //     });
    // },
// }