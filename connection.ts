import {createConnection} from 'mysql';

export const mysql = createConnection({
    host: 'localhost',
    user: 'root',
    password: 'admin',
    database: 'repuestock_appdev'
});

mysql.connect((err) => {
    if (err) throw err;
    console.log('Connect DB MYSQL');
});
